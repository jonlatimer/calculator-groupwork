﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorGroupWork
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public double Add(double x, double y)
        {
            double z = x + y;
            return z;
        }

        public double Subtract(double x, double y)
        {
            double Total = x - y;
            return Total;
        }

        public double Multiply(double x, double y)
        {
            double z = x * y;
            return z;
        }

        public double Divide(double x, double y)
        {
            double Total = x / y;
            return Total;
        }

        public int Factorial(int x)
        {
            int temno = 1;

            for (int i = 1; i <= x; i++)
            {
                temno = temno * i;
            }

            return temno;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
