﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorGroupWork;

namespace UnitTestCalculator
{
    [TestClass]
    public class UnitTest1
    {
        Form1 frm = new Form1();
        [TestMethod]
        public void TestAddition1()
        { 
            Assert.IsTrue(frm.Add(1, 1) == 2);
        }
        [TestMethod]
        public void TestAddition2()
        {
            Assert.IsTrue(frm.Add(1.25, 1) == 2.25);
        }
        [TestMethod]
        public void TestAddition3()
        {
            Assert.IsTrue(frm.Add(90.0, 90.1) == 180.1);
        }
        [TestMethod]
        public void TestSubtraction1()
        {
            Assert.IsTrue(frm.Subtract(90.0, 1) == 89);
        }
        [TestMethod]
        public void TestSubtraction2()
        {
            Assert.IsTrue(frm.Subtract(-1, 1) == -2);
        }
        [TestMethod]
        public void TestSubtraction3()
        {
            Assert.IsTrue(frm.Subtract(100, 1) == 99);
        }
        [TestMethod]
        public void TestMultiplication1()
        {
            Assert.IsTrue(frm.Multiply(100, 1) == 100);
        }
        [TestMethod]
        public void TestMultiplication2()
        {
            Assert.IsTrue(frm.Multiply(100.1, 10) == 1001);
        }
        [TestMethod]
        public void TestMultiplication3()
        {
            Assert.IsTrue(frm.Multiply(-5, -5) == 25);
        }
        [TestMethod]
        public void TestDivision1()
        {
            Assert.IsTrue(frm.Divide(25, 5) == 5);
        }
        [TestMethod]
        public void TestDivision2()
        {
            Assert.IsTrue(frm.Divide(10, 2.5) == 4);
        }
        [TestMethod]
        public void TestDivision3()
        {
            Assert.IsTrue(frm.Divide(1, 4) == 0.25);
        }
        [TestMethod]
        public void TestFactorial1()
        {
            Assert.IsTrue(frm.Factorial(4) == 24);
        }
        [TestMethod]
        public void TestFactorial2()
        {
            Assert.IsTrue(frm.Factorial(5) == 120);
        }
        [TestMethod]
        public void TestFactorial3()
        {
            Assert.IsTrue(frm.Factorial(6) == 720);
        }
    }
}
